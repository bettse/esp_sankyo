#include "SCT3Q8_Command.h"

SCT3Q8Command::SCT3Q8Command(uint8_t cm, uint8_t pm, uint8_t *data,
                             size_t length)
    : cm(cm), pm(pm), dataLen(length) {
  if (length > 0) {
    memcpy(raw + HEADER_LEN, data, length);
  }
}

String SCT3Q8Command::serialize() {
  FastCRC16 CRC16;

  size_t length = dataLen + 3;
  raw[0] = STX;
  raw[1] = (length >> 8) & 0xff;
  raw[2] = (length >> 0) & 0xff;
  raw[3] = 'C';
  raw[4] = cm;
  raw[5] = pm;
  // Data already copied in constructor

  uint16_t crc = CRC16.xmodem(raw, HEADER_LEN + dataLen);
  raw[HEADER_LEN + dataLen + 0] = crc >> 8;
  raw[HEADER_LEN + dataLen + 1] = crc >> 0;

  return String(raw, HEADER_LEN + dataLen + CRC_LEN);
}
