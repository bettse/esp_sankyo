#include "SCT3Q8_Response.h"

SCT3Q8Response::SCT3Q8Response(char *data, size_t length) {
  if (length < 5) {
    // Throw an error
    return;
  }

  success = (data[0] == 'P');
  cm = data[1];
  pm = data[2];
  if (success) {
    st1 = data[3];
    st0 = data[4];
  } else {
    e1 = data[3];
    e0 = data[4];
  }

  uint8_t start = 5;
  if (data[5] == 'X') {
    start++;
  }
  textLength = length - start;
  if (textLength > 0) {
    text = data + start;
  }
}

int SCT3Q8Response::statusCode() { return success ? 200 : 500; }

String SCT3Q8Response::serialize() {
  char buffer[250];

  jsonDocument.clear(); // Clear json buffer

  jsonDocument["success"] = success;

  jsonDocument["cm"] = String(cm);
  jsonDocument["pm"] = String(pm);
  if (success) {
    jsonDocument["statusCode"] = String(st1) + String(st0);
  } else {
    jsonDocument["errorCode"] = String(e1) + String(e0);
    jsonDocument["error"] = errorCodeToString();
  }

  if (textLength > 0) {
    jsonDocument["text"] = String(text, textLength);
    jsonDocument["data"] = String(text, textLength);
  }

  responseSpecificFields();

  serializeJson(jsonDocument, buffer);
  return String(buffer, strlen(buffer));
}

void SCT3Q8Response::responseSpecificFields() {
  switch (cm) {
  case CMD_STATUS:
    if (success) {
      if (st0 == '0') {
        jsonDocument["card_position"] = "none";
      } else if (st0 == '1') {
        jsonDocument["card_position"] = "gate";
      } else if (st0 == '2') {
        jsonDocument["card_position"] = "rf/ic"; // maybe name it "reader"?
      } else {
        jsonDocument["card_position"] = "unknown";
      }
    }
    break;
  }
}

String SCT3Q8Response::errorCodeToString() {
  String ec = String(e1) + String(e0);
  if (ec.equals("00")) {
    return String("A given command code is unidentified");
  } else if (ec.equals("01")) {
    return String("Parameter is not correct");
  } else if (ec.equals("02")) {
    return String("Command execution is impossible.");
  } else if (ec.equals("03")) {
    return String("Function is not implemented.");
  } else if (ec.equals("04")) {
    return String("Command data error");
  } else if (ec.equals("05")) {
    return String("Key for decrypting is not received");
  } else if (ec.equals("09")) {
    return String("Intake withdraw timeout");

  } else if (ec.equals("10")) {
    return String("Card jam");
  } else if (ec.equals("11")) {
    return String("Shutter error");
  } else if (ec.equals("13")) {
    return String("Irregular card length (LONG)");
  } else if (ec.equals("14")) {
    return String("Irregular card length (SHORT)");
  } else if (ec.equals("15")) {
    return String("Flash Memory Parameter Area CRC error");
  } else if (ec.equals("16")) {
    return String("Card position Move (and Pull out error)");
  } else if (ec.equals("17")) {
    return String("Jam error at retrieve");
  } else if (ec.equals("18")) {
    return String("Two card error");

  } else if (ec.equals("20")) {
    return String("Read Error (Parity error (VRC error))");
  } else if (ec.equals("21")) {
    return String(
        "Read Error (Start sentinel error, end sentinel error or LRC error)");
  } else if (ec.equals("23")) {
    return String("Read Error (No data contents, only start sentinel, end "
                  "sentinel and LRC)");
  } else if (ec.equals("24")) {
    return String("Read Error (No magnetic stripe or not encoded)");

  } else if (ec.equals("30")) {
    return String("Power Down");
  } else if (ec.equals("31")) {
    return String("DSR signal was turned to OFF");
  } else if (ec.equals("39")) {
    return String("Electric fan breaks down.");
  } else if (ec.equals("40")) {
    return String("Pull Out Error");

    // Some smartcard related errors in the 6x range I didn't copy

  } else if (ec.equals("70")) {
    return String("F-ROM write error");
  } else if (ec.equals("71")) {
    return String("CRC error of user program code area");
  } else if (ec.equals("B0")) {
    return String("Not receieved Initialize command");
  }

  return ec;
}
