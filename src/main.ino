#include "LittleFS.h"    // LittleFS is declared
#include <ArduinoJson.h> // https://github.com/bblanchon/ArduinoJson
#include <WiFi.h>

// needed for library
#include <WebServer.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager

#include <FastCRC.h>
#include <SCT3Q8_Command.h>
#include <SCT3Q8_Response.h>

#define RXD2 16
#define TXD2 17

#define ACK 0x06
#define NAK 0x15

#define LEN_LEN 2
#define CRC_LEN 2

// 300 including stx, 2 byte len, 2 byte crc
#define MAX_TEXT_LEN 295

#define WAIT_ACK_TIMEOUT 300
#define WAIT_RESP_TIMEOUT 20000
#define WAIT_LEN_TIMEOUT 250
#define WAIT_TEXT_TIMEOUT 250
#define WAIT_CRCC_TIMEOUT 250
#define HTTP_TIMEOUT 10000

#define IN "<= "
#define OUT "=> "

enum Mode { WAIT_ACK, WAIT_RESP, WAIT_LEN, WAIT_TEXT, WAIT_CRCC } mode;

bool shouldSaveConfig = false;
long previousMillis = 0;
long timeout = 1000;

uint8_t header[1] = {STX};

uint8_t lenBytes[LEN_LEN] = {0};
size_t lenCount = 0;
uint16_t len = 0;

char textBytes[MAX_TEXT_LEN] = {0};
size_t textCount = 0;

uint8_t crcBytes[CRC_LEN] = {0};
size_t crcCount = 0;

WebServer server(80);
FastCRC16 CRC16;

void saveConfigCallback() {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void setupLittleFS() {
  // read configuration from FS json
  Serial.println("mounting FS...");

  if (LittleFS.begin()) {
    Serial.println("mounted file system");
    if (LittleFS.exists("/config.json")) {
      // file exists, reading and loading
      Serial.println("reading config file");
      File configFile = LittleFS.open("/config.json", "r");
      if (configFile) {
        Serial.println("opened config file");
        size_t size = configFile.size();
        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[size]);

        configFile.readBytes(buf.get(), size);
        DynamicJsonDocument jsonBuffer(1024);
        deserializeJson(jsonBuffer, buf.get());
        serializeJson(jsonBuffer, Serial);
        if (jsonBuffer.isNull()) {
          Serial.println("failed to load json config");
        } else {
          Serial.println("\nparsed json");
        }
      }
    }
  } else {
    Serial.println("failed to mount FS");
  }
}

void logsend(const uint8_t *buf, size_t count) {
  Serial.print(OUT);
  for (size_t i = 0; i < count; i++) {
    Serial.printf("%02x", buf[i]);
  }
  Serial.println();
  Serial2.write(buf, count);
}

StaticJsonDocument<250> jsonDocument;
char buffer[250];

void getRoot() {
  Serial.println("Get::Root");
  server.send(200, "application/json", "{}");
}

void postInit() {
  Serial.println("POST::Init");
  jsonDocument.clear(); // Clear json buffer

  char init_data[] = "32400001010";
  SCT3Q8Command init(CMD_INIT, '0', (uint8_t *)init_data, strlen(init_data));

  String s = init.serialize();
  logsend((uint8_t *)s.c_str(), s.length());

  waitForResponse();
  SCT3Q8Response r(textBytes, strlen(textBytes));
  server.send(r.statusCode(), "application/json", r.serialize());
}

void handle_NotFound() {
  Serial.println("NotFound");
  server.send(404, "application/json", "{}");
}

void postShutter() {
  Serial.println("POST::Shutter");
  if (server.hasArg("plain")) {
    String body = server.arg("plain");
    deserializeJson(jsonDocument, body);

    bool enable = jsonDocument["enable"];
    jsonDocument.clear(); // Clear json buffer

    if (enable) {
      SCT3Q8Command move(CMD_SHUTTER, '0', NULL, 0);
      String s = move.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
    } else {
      SCT3Q8Command move(CMD_SHUTTER, '1', NULL, 0);
      String s = move.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
    }
    if (waitForResponse()) {
      SCT3Q8Response r(textBytes, strlen(textBytes));
      server.send(r.statusCode(), "application/json", r.serialize());
    }
  }

  server.send(422, "application/json", "{}");
}

void postMove() {
  Serial.println("POST::Move");
  if (server.hasArg("plain")) {
    String body = server.arg("plain");
    deserializeJson(jsonDocument, body);

    String action = jsonDocument["action"];
    jsonDocument.clear(); // Clear json buffer
    action.toLowerCase();
    Serial.printf("action: %s\n", action.c_str());

    // from hopper: should this be a 'move', or something different
    if (action.equals("load")) {
      SCT3Q8Command move(CMD_ENTRY, '2', NULL, 0);
      String s = move.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
    } else if (action.equals("eject")) {
      SCT3Q8Command move(CMD_CARRY, '0', NULL, 0);
      String s = move.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
    } else if (action.equals("capture")) {
      SCT3Q8Command move(CMD_CARRY, '1', NULL, 0);
      String s = move.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
    } else if (action.equals("retrieve")) {
      SCT3Q8Command move(CMD_RETRIEVE, '0', NULL, 0);
      String s = move.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
    } else if (action.equals("withdraw")) {
      // TODO: add optional timeout
      SCT3Q8Command move(CMD_INTAKE, '0', NULL, 0);
      String s = move.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
    } else if (action.equals("intake")) {
      // TODO: add optional timeout
      SCT3Q8Command move(CMD_INTAKE, '1', NULL, 0);
      String s = move.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
    } else {
      server.send(422, "application/json", "{}");
      return;
    }

    if (waitForResponse()) {
      SCT3Q8Response r(textBytes, strlen(textBytes));
      server.send(r.statusCode(), "application/json", r.serialize());
    }
  }

  server.send(422, "application/json", "{}");
}

bool waitForResponse() {
  unsigned long expiration = millis() + HTTP_TIMEOUT;
  do {
    if (getResponse()) {
      return true;
    }
  } while (millis() < expiration);
  return false;
}

void getStatus() {
  Serial.println("GET::Status");
  jsonDocument.clear(); // Clear json buffer

  SCT3Q8Command command(CMD_STATUS, '1', NULL, 0);
  String s = command.serialize();
  logsend((uint8_t *)s.c_str(), s.length());

  waitForResponse();
  SCT3Q8Response r(textBytes, strlen(textBytes));

  server.send(r.statusCode(), "application/json", r.serialize());
}

void getMagstripe() {
  Serial.println("GET::Magstripe");
  jsonDocument.clear(); // Clear json buffer

  uint8_t pm = '0';
  int paramsNr = server.args();
  if (paramsNr > 0) {
    String value = server.arg("track");
    if (value.equals("1")) {
      pm = '1';
    } else if (value.equals("2")) {
      pm = '2';
    } else if (value.equals("3")) {
      pm = '3';
    } else if (value.equals("all")) {
      pm = '5';
    } else if (value.equals("clear")) {
      pm = '6';
    }
  }

  SCT3Q8Command command(CMD_MAG_READ, pm, NULL, 0);
  String s = command.serialize();
  logsend((uint8_t *)s.c_str(), s.length());

  waitForResponse();
  SCT3Q8Response r(textBytes, strlen(textBytes));
  server.send(r.statusCode(), "application/json", r.serialize());
}

void postMagstripe() {
  Serial.println("POST::Magstripe");
  if (server.hasArg("plain")) {
    String body = server.arg("plain");
    deserializeJson(jsonDocument, body);

    JsonVariant trackNum = jsonDocument["track"];
    if (trackNum.isNull()) {
      server.send(422, "application/json", "{}");
      return;
    }

    JsonVariant content = jsonDocument["content"];
    if (content.isNull()) {
      server.send(422, "application/json", "{}");
      return;
    }
    jsonDocument.clear(); // Clear json buffer

    uint8_t pm = 0x30 + trackNum.as<uint8_t>();
    String param = content.as<String>();

    SCT3Q8Command command('7', pm, (uint8_t *)param.c_str(), param.length());
    String s = command.serialize();
    logsend((uint8_t *)s.c_str(), s.length());

    waitForResponse();
    SCT3Q8Response r(textBytes, strlen(textBytes));
    server.send(r.statusCode(), "application/json", r.serialize());
  }
  server.send(500, "application/json", "{}");
}

void postLed() {
  Serial.println("POST::Led");
  if (server.hasArg("plain")) {
    String body = server.arg("plain");
    deserializeJson(jsonDocument, body);

    // TODO: Add blinking support
    bool red = jsonDocument["red"];
    bool green = jsonDocument["green"];
    bool orange = jsonDocument["orange"];
    jsonDocument.clear(); // Clear json buffer

    bool states[4] = {true, green, red, orange};

    for (size_t i = 0; i < sizeof(states); i++) {
      // Skip any that were marked false
      if (!states[i]) {
        continue;
      }

      uint8_t pm = 0x30 + i;

      SCT3Q8Command command('5', pm, NULL, 0);
      String s = command.serialize();
      logsend((uint8_t *)s.c_str(), s.length());
      waitForResponse();

      SCT3Q8Response r(textBytes, strlen(textBytes));
      if (!r.success) {
        server.send(r.statusCode(), "application/json", r.serialize());
        return;
      }
    }
  }
  SCT3Q8Response r(textBytes, strlen(textBytes));
  server.send(r.statusCode(), "application/json", r.serialize());
}

void setupWebserver() {
  // https://github.com/espressif/esp-idf/issues/7328
  esp_wifi_set_ps(WIFI_PS_NONE);
  // lru_purge_enable = true;

  server.on("/", HTTP_GET, getRoot);
  server.on("/", HTTP_POST, postInit);

  server.on("/status", HTTP_GET, getStatus);
  server.on("/move", HTTP_POST, postMove);
  server.on("/shutter", HTTP_POST, postShutter);
  server.on("/magstripe", HTTP_GET, getMagstripe);
  server.on("/magstripe", HTTP_POST, postMagstripe);
  // Doesn't seem to work with my model
  // server.on("/led", HTTP_POST, postLed);

  server.onNotFound(handle_NotFound);
  server.enableDelay(1);
  server.enableCORS();
  server.enableCrossOrigin();
  server.begin();
}

void setup() {
  Serial.begin(115200);
  pinMode(TXD2, OUTPUT);
  pinMode(RXD2, INPUT);
  Serial2.begin(9600, SERIAL_8E1, RXD2, TXD2);

  setupLittleFS();

  WiFiManager wifiManager;
  wifiManager.setSaveConfigCallback(saveConfigCallback);
  wifiManager.autoConnect("AutoConnectAP");
  Serial.println("Connected");

  if (shouldSaveConfig) {
    Serial.println("saving config");
    DynamicJsonDocument jsonBuffer(1024);

    File configFile = LittleFS.open("/config.json", "w");
    if (!configFile) {
      Serial.println("failed to open config file for writing");
    }

    serializeJsonPretty(jsonBuffer, Serial);
    serializeJson(jsonBuffer, configFile);
    configFile.close();
    // end save
    shouldSaveConfig = false;
  }

  setupWebserver();
}

void resetTimeout() { previousMillis = millis(); }

void setTimeout(long newTimeout) {
  timeout = newTimeout;
  resetTimeout();
}

bool getResponse() {
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis > timeout) {
    previousMillis = currentMillis;

    switch (mode) {
    case WAIT_ACK:
      // resend command
      mode = WAIT_ACK;
      break;
    case WAIT_RESP:
      // resend command
      Serial.println("Timeout: reset to WAIT_ACK");
      mode = WAIT_ACK;
      break;
    case WAIT_LEN:
    case WAIT_TEXT:
    case WAIT_CRCC:
      Serial.println("Timeout: reset to WAIT_RESP");
      Serial2.write(NAK);
      mode = WAIT_RESP;
      break;
    default:
      Serial.println("Unknown mode");
    }
  }

  while (Serial2.available()) {
    char c = Serial2.read();

    switch (mode) {
    case WAIT_ACK:
      if (c == ACK) {
        Serial.println(IN "ACK");
        setTimeout(WAIT_RESP_TIMEOUT);
        mode = WAIT_RESP;
      } else {
        Serial.println(c, HEX);
      }
      break;
    case WAIT_RESP:
      if (c == STX) {
        Serial.println(IN "STX");
        CRC16.xmodem(header, sizeof(header));
        setTimeout(WAIT_LEN_TIMEOUT);
        mode = WAIT_LEN;
      } else {
        Serial.println(c, HEX);
      }
      break;
    case WAIT_LEN:
      if (lenCount < LEN_LEN) {
        lenBytes[lenCount++] = c;
      }
      if (lenCount == LEN_LEN) {
        len = lenBytes[0] << 8 | lenBytes[1];
        // Serial.printf("Length %d\n", len);
        CRC16.xmodem_upd(lenBytes, LEN_LEN);
        setTimeout(WAIT_TEXT_TIMEOUT);
        mode = WAIT_TEXT;

        lenCount = 0;
        memset(lenBytes, 0, LEN_LEN);
      }
      break;
    case WAIT_TEXT:
      if (textCount == 0) {
        memset(textBytes, 0, MAX_TEXT_LEN);
      }
      if (textCount < MAX_TEXT_LEN) {
        textBytes[textCount++] = c;
      }
      if (textCount == len) {
        Serial.printf("%s %s\n", IN, textBytes);
        // Serial.println("Got length bytes, waiting for CRC");
        CRC16.xmodem_upd((uint8_t*)textBytes, len);
        setTimeout(WAIT_CRCC_TIMEOUT);
        mode = WAIT_CRCC;

        textCount = 0;
      }
      break;
    case WAIT_CRCC:
      if (crcCount == 0) {
        memset(crcBytes, 0, CRC_LEN);
      }
      if (crcCount < CRC_LEN) {
        crcBytes[crcCount++] = c;
      }
      if (crcCount == CRC_LEN) {
        uint16_t valid_crc = CRC16.xmodem_upd(crcBytes, CRC_LEN);
        if (valid_crc == 0) {
          Serial.println(OUT "ACK");
          Serial2.write(ACK);

          setTimeout(WAIT_ACK_TIMEOUT);
          mode = WAIT_ACK;
          crcCount = 0;

          return true;
        } else {
          Serial.printf("Invalid CRC %02X%02X\n", crcBytes[0], crcBytes[1]);
          Serial.printf("%s\n", textBytes);
        }

        crcCount = 0;
      }
      break;
    default:
      Serial.printf("Unknown mode\n");
    }
  }

  return false;
}

void loop() {
  delay(1);
  server.handleClient();
}
