#ifndef SCT3Q8_COMMAND_H
#define SCT3Q8_COMMAND_H

#include <Arduino.h>

#include <cstdio>
#include <cstddef>
#include <cstdint>

#include <cstring>
#include <cstdlib>

#include <FastCRC.h>

#define STX 0xF2

#define MAX_COMMAND_LEN 300
#define CRC_LEN 2
#define HEADER_LEN 6

class SCT3Q8Command {
private:
    uint8_t cm;
    uint8_t pm;
    size_t dataLen;
    uint8_t raw[MAX_COMMAND_LEN] = {0};

public:
    SCT3Q8Command(uint8_t cm, uint8_t pm, uint8_t* data, size_t length);

    String serialize();
};

#endif
