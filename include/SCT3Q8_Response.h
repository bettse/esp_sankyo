#ifndef SCT3Q8_RESPONSE_H
#define SCT3Q8_RESPONSE_H

#include <Arduino.h>
#include <ArduinoJson.h> // https://github.com/bblanchon/ArduinoJson

#include <cstdio>
#include <cstddef>
#include <cstdint>

#include <cstring>
#include <cstdlib>

enum Command {
  CMD_INIT = '0',
  CMD_STATUS = '1',
  CMD_ENTRY = '2',
  CMD_CARRY = '3',
  CMD_RETRIEVE = '4',
  CMD_LED = '5',
  CMD_MAG_READ = '6',
  CMD_MAG_WRITE = '7',
  CMD_INTAKE = '9',
  CMD_SHUTTER = ':'
};

class SCT3Q8Response {
private:
  char cm;
  char pm;
  char e1;
  char e0;
  char st1;
  char st0;
  String errorMessage;
  StaticJsonDocument<250> jsonDocument;

  size_t textLength;
  char * text;

  String errorCodeToString();
  void responseSpecificFields();

public:
  bool success;
  SCT3Q8Response(char *data, size_t length);

  int statusCode();
  String serialize();
};

#endif
